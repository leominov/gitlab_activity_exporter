package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

var (
	listenAddress    = flag.String("web.listen-address", ":9201", "Address to listen on for web interface and telemetry.")
	metricsPath      = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
	logLevel         = flag.String("log-level", "INFO", "Level of logging.")
	printsVersion    = flag.Bool("version", false, "Prints version and exit.")
	cacheTTL         = flag.Duration("cache-ttl", 0, "Cache TTL for users activity data.")
	inactiveGroupMap = flag.String("max-inactive", "0", "Named groups with maximum allowed inactive time for users (ex: inactive=30m,pretender=15m).")
)

func main() {
	flag.Parse()

	if *printsVersion {
		fmt.Println(Version)
		return
	}

	// Output logs in JSON format
	logrus.SetFormatter(&logrus.JSONFormatter{})

	if level, err := logrus.ParseLevel(*logLevel); err == nil {
		logrus.SetLevel(level)
	}

	config, err := LoadConfigFromEnv()
	if err != nil {
		logrus.Fatal(err)
	}

	groupMap, err := ParseInactiveGroupMap(*inactiveGroupMap)
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Infof("Staging gitlab_activity_exporter %s...", Version)
	logrus.Debugf("Configuration: %s", config.String())
	logrus.Debugf("Inactive group map: %v", groupMap)
	if *cacheTTL > 0 {
		logrus.Debugf("Cache TTL: %s", cacheTTL.String())
	}

	logrus.Infof("Listening on address: %s", *listenAddress)
	http.Handle(*metricsPath, promhttp.Handler())

	exporter, err := NewExporter(config, groupMap, *cacheTTL)
	if err != nil {
		logrus.WithError(err).Fatal("Error creating an exporter")
	}

	err = prometheus.Register(exporter)
	if err != nil {
		logrus.WithError(err).Fatal("Error registering an collector")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>GitLab Activity Exporter</title></head>
             <body>
             <h1>GitLab Activity Exporter</h1>
             <p><a href='` + *metricsPath + `'>Metrics</a></p>
             </body>
             </html>`))
	})
	if err := http.ListenAndServe(*listenAddress, nil); err != nil {
		logrus.WithError(err).Fatal("Error starting HTTP server")
	}
}

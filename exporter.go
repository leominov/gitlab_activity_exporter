package main

import (
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

const namespace = "gitlab_activity_exporter"

type Exporter struct {
	config    *Config
	gitlabCli *gitlab.Client
	groupMap  map[string]time.Duration

	cacheTTL       time.Duration
	cacheUpdatedAt time.Time
	cachedUsers    []*gitlab.User
	cachedError    error

	lastActivity    *prometheus.Desc
	maxInactive     *prometheus.Desc
	lastScrapeError prometheus.Gauge
	totalScrapes    prometheus.Counter
}

func NewExporter(c *Config, groupMap map[string]time.Duration, cacheTTL time.Duration) (*Exporter, error) {
	e := &Exporter{
		config:   c,
		cacheTTL: cacheTTL,
		groupMap: groupMap,
		lastActivity: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "user", "last_activity_seconds"),
			"Last user's activity.",
			[]string{"inactive_group", "email", "username"},
			nil,
		),
		maxInactive: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "settings", "max_inactive_seconds"),
			"Value of max-inactive setting.",
			[]string{"inactive_group"},
			nil,
		),
		totalScrapes: prometheus.NewCounter(prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "scrapes_total",
			Help:      "Current total scrapes.",
		}),
		lastScrapeError: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "last_scrape_error",
			Help:      "The last scrape error status.",
		}),
	}
	err := e.setupGitLabClient()
	return e, err
}

func (e *Exporter) setupGitLabClient() error {
	var (
		cli *gitlab.Client
		err error
	)
	if len(e.config.GitlabToken) > 0 {
		cli, err = gitlab.NewClient(e.config.GitlabToken, gitlab.WithBaseURL(e.config.GitlabEndpoint))
	} else {
		cli, err = gitlab.NewBasicAuthClient(e.config.GitlabUsername, e.config.GitlabPassword, gitlab.WithBaseURL(e.config.GitlabEndpoint))
	}
	e.gitlabCli = cli
	return err
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- e.totalScrapes.Desc()
	ch <- e.lastScrapeError.Desc()
	ch <- e.maxInactive
	ch <- e.lastActivity
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	e.scrape(ch)

	ch <- e.totalScrapes
	ch <- e.lastScrapeError
}

func (e *Exporter) getCachedUsers() ([]*gitlab.User, bool, error) {
	var expired bool
	if time.Since(e.cacheUpdatedAt) > e.cacheTTL {
		expired = true
		e.cachedUsers = []*gitlab.User{}
		e.cachedError = nil
	}
	return e.cachedUsers, expired, e.cachedError
}

func (e *Exporter) setCachedUsers(users []*gitlab.User, err error) {
	e.cacheUpdatedAt = time.Now()
	e.cachedError = err
	e.cachedUsers = users
}

func (e *Exporter) GetUsers() ([]*gitlab.User, error) {
	users, expired, err := e.getCachedUsers()
	if !expired {
		return users, err
	}
	users, err = e.getUsers()
	e.setCachedUsers(users, err)
	return users, err
}

func (e *Exporter) scrape(ch chan<- prometheus.Metric) {
	e.totalScrapes.Inc()
	e.lastScrapeError.Set(0)

	for groupName, groupDuration := range e.groupMap {
		d := groupDuration.Seconds()
		ch <- prometheus.MustNewConstMetric(e.maxInactive, prometheus.GaugeValue, d, []string{
			groupName,
		}...)
	}

	users, err := e.GetUsers()
	if err != nil {
		logrus.WithError(err).Error("Failed to fetch user list")
		e.lastScrapeError.Set(1)
		return
	}

	for _, user := range users {
		for groupName, groupDuration := range e.groupMap {
			if !e.isReportableUser(user, groupDuration) {
				continue
			}
			lastActivityUnix := float64(time.Time(*user.LastActivityOn).Unix())
			ch <- prometheus.MustNewConstMetric(e.lastActivity, prometheus.GaugeValue, lastActivityUnix, []string{
				groupName,
				user.Email,
				user.Username,
			}...)
		}
	}
}

func (e *Exporter) isReportableUser(user *gitlab.User, d time.Duration) bool {
	if user.LastActivityOn == nil {
		return false
	}
	if d == 0 {
		return true
	}
	lastActivity := time.Since(time.Time(*user.LastActivityOn))
	return lastActivity > d
}

func (e *Exporter) getUsers() ([]*gitlab.User, error) {
	var users []*gitlab.User
	page := 1
	for {
		options := &gitlab.ListUsersOptions{
			ListOptions: gitlab.ListOptions{
				Page: page,
			},
			Active: gitlab.Bool(true),
		}
		fetchedUsers, r, err := e.gitlabCli.Users.ListUsers(options)
		if err != nil {
			return nil, err
		}
		users = append(users, fetchedUsers...)
		nextPageRaw := r.Header.Get("X-Next-Page")
		if len(nextPageRaw) == 0 {
			break
		}
		nextPage, err := strconv.Atoi(nextPageRaw)
		if err != nil {
			break
		}
		page = nextPage
	}
	return users, nil
}

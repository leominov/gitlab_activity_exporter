package main

import (
	"reflect"
	"testing"
	"time"
)

func TestParseInactiveGroupMap(t *testing.T) {
	tests := []struct {
		in      string
		out     map[string]time.Duration
		wantErr bool
	}{
		{
			in: "1s",
			out: map[string]time.Duration{
				"all": time.Second,
			},
			wantErr: false,
		},
		{
			in: "all=0",
			out: map[string]time.Duration{
				"all": 0,
			},
			wantErr: false,
		},
		{
			in:      "all",
			wantErr: true,
		},
		{
			in:      "inactive=30m,pretender",
			wantErr: true,
		},
		{
			in: "inactive=30m,pretender=15m",
			out: map[string]time.Duration{
				"inactive":  30 * time.Minute,
				"pretender": 15 * time.Minute,
			},
			wantErr: false,
		},
		{
			in: "30m,pretender=15m",
			out: map[string]time.Duration{
				"all":       30 * time.Minute,
				"pretender": 15 * time.Minute,
			},
			wantErr: false,
		},
	}
	for _, test := range tests {
		out, err := ParseInactiveGroupMap(test.in)
		if test.wantErr != (err != nil) {
			t.Errorf("Must be %v, but got %v", test.wantErr, err)
		}
		if !reflect.DeepEqual(test.out, out) {
			t.Errorf("Must be %v, but git %v", test.out, out)
		}
	}
}

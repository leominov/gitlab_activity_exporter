module github.com/leominov/gitlab_activity_exporter

go 1.14

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/client_golang v1.5.1
	github.com/sirupsen/logrus v1.5.0
	github.com/xanzy/go-gitlab v0.31.0
)

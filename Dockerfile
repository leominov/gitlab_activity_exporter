FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/github.com/leominov/gitlab_activity_exporter
COPY . .
RUN go build -o gitlab_activity_exporter ./

FROM alpine:3.11
COPY --from=builder /go/src/github.com/leominov/gitlab_activity_exporter/gitlab_activity_exporter /usr/local/bin/gitlab_activity_exporter
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["gitlab_activity_exporter"]

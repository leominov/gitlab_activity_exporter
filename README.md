# GitLab Activity Exporter

## Environment variables

* `GITLAB_ENDPOINT` (ex.: `https://gitlab.local/api/v4`)
* `GITLAB_TOKEN`
* `GITLAB_USERNAME`
* `GITLAB_PASSWORD`

## Usage

```shell
Usage of ./gitlab_activity_exporter:
  -cache-ttl duration
        Cache TTL for users activity data.
  -log-level string
        Level of logging. (default "INFO")
  -max-inactive duration
        Named groups with maximum allowed inactive time for users (ex: inactive=720h,pretender=336h).
  -version
        Prints version and exit.
  -web.listen-address string
        Address to listen on for web interface and telemetry. (default ":9201")
  -web.telemetry-path string
        Path under which to expose metrics. (default "/metrics")
```

Without `-max-inactive` all users will be exposed.

## Metrics

* `gitlab_activity_exporter_last_scrape_error`
* `gitlab_activity_exporter_scrapes_total`
* `gitlab_activity_exporter_settings_max_inactive_seconds` by (inactive_group, name, email)
* `gitlab_activity_exporter_user_last_activity_seconds` by (inactive_group)

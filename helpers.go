package main

import (
	"strings"
	"time"
)

func parseGroupPair(groupPair []string) (name string, d time.Duration, err error) {
	var durationRaw string
	if len(groupPair) == 1 {
		name = "all"
		durationRaw = groupPair[0]
	} else {
		name = groupPair[0]
		durationRaw = groupPair[1]
	}
	d, err = time.ParseDuration(durationRaw)
	return name, d, err
}

func ParseInactiveGroupMap(inactiveGroupMap string) (map[string]time.Duration, error) {
	groups := make(map[string]time.Duration)
	groupsIn := strings.Split(inactiveGroupMap, ",")
	for _, group := range groupsIn {
		group = strings.TrimSpace(strings.ToLower(group))
		groupPair := strings.Split(group, "=")
		groupName, groupDuration, err := parseGroupPair(groupPair)
		if err != nil {
			return nil, err
		}
		groups[groupName] = groupDuration
	}
	return groups, nil
}
